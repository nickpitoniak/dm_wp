<?php

function fetch_srp_for_dealership($dealership_email_address) {
  // POST dealership id to IMS server
  $ch = curl_init("http://35.192.130.76/fetch-dealership-dashboard");
  $payload = json_encode( array( "dealership_email_address"=> $dealership_email_address ) );
  curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
  curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}
function fetch_vdp_for_dealership($dealership_email_address,$inventory_vin) {
  // POST dealership id to IMS server
  $ch = curl_init("http://35.192.130.76/fetch-inventory-record");
  $payload = json_encode( array( "dealership_email_address"=> $dealership_email_address, "inventory_vin"=>$inventory_vin ) );
  curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
  curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  $result = curl_exec($ch);
  curl_close($ch);
  return $result;
}
function get_dealership_email() {
  return trim(file_get_contents("./wp-content/themes/twentytwenty/dealership.txt"));
}
function display_lead_form() {
$lead_code = <<<EOD
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<div style="margin:20px; width:33.3%; display:inline-block; float:left;border:5px solid rgba(241, 196, 15,0.15);background-color:#ecf0f1;-webkit-box-shadow: 0.5px 0.5px 1px 0.5px #95a5a6;-moz-box-shadow: 0.5px 0.5px 1px 0.5px #95a5a6;box-shadow: 0.5px 0.5px 1px 0.5px #95a5a6;">
  <div style="padding:10px;">
    <h4>Lead Form Indicating Customer Interest (WP shortcode placable anywhere on website)</h4>
    <div style="width:49%; display:inline-block">
      <p style="margin-bottom:0">Customer First Name</p>
      <input type="text" class="form-control" placeholder="Abraham" id="customer_first_name">
    </div>
    <div style="width:49%; display:inline-block">
      <p style="margin-bottom:0">Customer Phone Number</p>
      <input type="text" class="form-control" placeholder="(717) 482-2211" id="customer_phone_number">
    </div>
    <p style="margin-bottom:0; margin-top:10">Customer Email Address</p>
    <input type="text" class="form-control" placeholder="sarah@googlemail.com" id="customer_email_address" style="margin-bottom:10">
    <div style="width:49%; display:inline-block">
      <p style="margin-bottom:0">Vin</p>
      <input type="text" class="form-control" readonly="readonly" id="vehicle_vin" value="5UXKR2C32H0U23043"> 
    </div>
    <div style="width:49%; display:inline-block">
      <p style="margin-bottom:0">Make</p>
      <input type="text" class="form-control" readonly="readonly" id="vehicle_make" value="Kia">
    </div>
    <div style="width:49%; display:inline-block;margin-top:20px">
      <p style="margin-bottom:0">Model</p>
      <input type="text" class="form-control" readonly="readonly" id="vehicle_model" value="Sporte">
    </div>
    <div style="width:49%; display:inline-block;margin-top:20px">
      <p style="margin-bottom:0">Year</p>
      <input type="text" class="form-control" readonly="readonly" id="vehicle_year" value="2015">
          </div>
    <p style="margin-top:20px;margin-bottom:0px;">Comments</p>
    <textarea class="form-control" rows="4" id="comments" placeholder="I am very interested in this vehicle please let me know as soon as possible if it is available but only outside of regular business hours"></textarea>
    <button onclick="submit_lead()" style="background-color:#27ae60; color:white; margin-top:20px;" class="btn">Submit Lead!</button>
  </div>
</div>
<script type="text/javascript">
function submit_lead() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);
      document.getElementById("customer_first_name").value = "";
      document.getElementById("customer_email_address").value = "";
      document.getElementById("customer_phone_number").value = "";
      document.getElementById("comments").value = "";
    }
  };
  xhttp.open("GET", "/?cfn="+String(document.getElementById("customer_first_name").value)+"&cpn="+String(document.getElementById("customer_phone_number").value)+"&cea="+String(document.getElementById("customer_email_address").value)+"&vv="+String(document.getElementById("vehicle_vin").value)+"&vmake="+String(document.getElementById("vehicle_make").value)+"&vmodel="+String(document.getElementById("vehicle_model").value)+"&vyr="+String(document.getElementById("vehicle_year").value)+"&comments="+String(document.getElementById("comments").value), true);
  xhttp.send();
}
</script>
EOD;
echo $lead_code;
}
if(count($_GET) == 0 && count($_POST) == 0) {
  echo "<a href='/?p=srp'>srp</a>";
  echo "<br>";
  echo "<a href='/?lead=true'>submit lead</a>";
}
if(isset($_GET['cfn'])) {
  $ch = curl_init( "http://34.106.81.56/submit-lead" );
  $payload = json_encode(array('dealership_email_address'=>get_dealership_email(),'customer_first_name'=>$_GET["cfn"],'customer_phone_number'=>$_GET["cpn"],'customer_email_address'=>$_GET["cea"],'vehicle_vin'=>$_GET["vv"],'vehicle_make'=>$_GET["vmake"],'vehicle_model'=>$_GET["vmodel"],'vehicle_year'=>$_GET['vyr'],'comments'=>$_GET['comments']));
  curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
  curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  $result = curl_exec($ch);
  curl_close($ch);
  print_r($result);
  exit;
}
if(isset($_GET['lead'])) {
  display_lead_form();
}
if(isset($_GET['p'])) {  // display custom screen { srv,vdp,vcp }
  if($_GET['p'] == "srp") {  // display SRP
    echo "SRP PAGE ".get_dealership_email().":)<br><br>";
    $srp_data = fetch_srp_for_dealership(get_dealership_email());
    $info = json_decode($srp_data);
    if(isset($info->inventory_record) && isset($info->inventory_record->inventory_records)) { print_r($info->inventory_record->inventory_records); } else { echo "no inventory present"; }
  } // end SRP interface output
  if($_GET['p'] == "vdp") {  // display SRP
    echo "VDP PAGE ".get_dealership_email().":)";
    $vdp_data = fetch_vdp_for_dealership(get_dealership_email(),$_GET['v']);
    $info = json_decode($vdp_data);
    print_r($info);
  } // end VDP interface output
  if($_GET['p'] == "vcp") {  // display SRP
    echo "VCP PAGE ".get_dealership_email().":)";
  } // end VCP interface output
}